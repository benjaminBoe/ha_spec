'''
file that runs the entire code and plots the results, can save previous runs and mess with plots using savez stuff
'''

import sys
sys.path.append('/Users/CommanderData/Documents/Grad_School/Fall_2015/RA/program_files/')

import runFunction as run
import numpy as np
import matplotlib.pyplot as plt  
reload(run)

#type out folder location
fileLocation = '/Users/CommanderData/Documents/Grad_School/Fall_2015/RA/Spectra_data/'   

#if you want to run the full program Ran = False, if you just want to mess with plots Ran = True
Ran = True
if Ran == False:
    runCode = run.runCode(fileLocation)
    np.savez(fileLocation+'runCode.npz', runCode = runCode)
else:
    data = np.load(fileLocation+'runCode.npz')
    runCode = data['runCode']
finalSpecs = runCode[0] 
finalSols = runCode[1]
specList = runCode[2]
sol = runCode[3]
yplot = runCode[4]
normSpec = runCode[5]
stepsToTake = runCode[6]
amountOfCorr = runCode[7]


plotFig1 = False
if plotFig1 == True:
#figure 1 plots the first of the original spectra with the 20 fitting points and the polynomial fit
    numOfSpec = len(specList)
    fig = plt.figure(frameon=False)   
    fig.set_size_inches(50,10)        
    plt.xlabel('Wavelength, index', fontsize = 50)                 #setting axis labels for later plot
    plt.ylabel('Light Intensity', fontsize = 50)  
    plt.title('Input spectra and polynomial fit')
    x = np.linspace(0,len(specList[0]),num = len(specList[0]))
    plt.plot(x,specList[0])                            #plots the new shifted spectra


    x = np.linspace(0,len(specList[0]),num = len(yplot[0]))
    plt.plot(x,yplot[0])
    plt.ylim((0,200000))   
    plt.xlim((0,4000))

plotFig2 = False
if plotFig2 == True:
    #figure 2 plots the first normalized spectra 
    fig2 = plt.figure(frameon=False)   
    fig2.set_size_inches(20,10)        #set figure size for high resolution viewing
    plt.plot(sol,normSpec)
    plt.title('Normalized and cropped spec')
    plt.xlabel('Wavelength, A', fontsize = 50)                 #setting axis labels for later plot
    plt.ylabel('Light Intensity', fontsize = 50)  
    plt.ylim((0,1.5))   
    plt.xlim(6550,6590)
    
numOfSpec = len(specList)


plotFig3= False
if plotFig3 ==True:
    fig3 = plt.figure(frameon=False)
    fig3.set_size_inches(20,10) 
    window = len(amountOfCorr[0])

    x = np.linspace(-window/2.,window/2., num = window)
    for jj in range (0, numOfSpec):
        y = amountOfCorr[jj]
        plt.plot(x,y)
        #plt.ylim(min(y) - 100, max(y) + 100)
        plt.xlabel('hello')
    

#fig5 = plt.figure()
#sizeOfWindow = len(interpCorr)
#x = np.linspace(0,sizeOfWindow, num = sizeOfWindow)
#for jj in range (0, numOfSpec):
 #   y = interpCorr[jj]
  #  plt.plot(x,y)
        
        
fig6 = plt.figure()
for jj in range (0, numOfSpec): 
    plt.xlim(6650,6500)
    #plt.ylim(0,1.5)   
    plt.xlabel('Wav, index', fontsize = 50)                 #setting axis labels for later plot
    plt.ylabel('Light Intensity', fontsize = 50)  
    fig6.set_size_inches(10,10)
    plt.plot(finalSols[jj],finalSpecs[jj])
    
plt.show()
