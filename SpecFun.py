from astropy.io import fits 
from   os import listdir
import numpy as np
from scipy.interpolate import interp1d

'''
readInFiles function takes the i-1 band fits file data out of a folder for both the spectra and the keck wavelength solution

inputs:
    filelocation- string that contains the path to the desired folder
    
return:
    sol - is the wavelength solution data in a list
    spec - is a list of spectra data lists

'''


def readInFiles(filelocation):
    
     #generates list to be used for outputing spectra data
    spec = []               
    
    #initilizes a counter for keeping track of the number of files imported                     
    ii = 0                     
    
    #goes through the folder and finds fits files to be loaded                                 
    for filename in listdir(filelocation):   
        
        #selescts out only fits files
        if filename.endswith(".fits"):
                  
               
            #selescts out the i band from the keck solutions
            if filename.startswith("keck_i"):      
                    
                #file location with the file name to be loaded
                name = filelocation + filename       
                    
                #first entry in output is now the solution 
                sol = ((fits.open(name)[0]).data)[0]      
                    
           
           #selescts out all i band spectrum data
            elif filename.startswith('i'):   
                          
                #file location and name to be loaded    
                name = filelocation + filename  
                
                #loads in the spectra data as a list into a new entry in the spec list
                spec.append(0)
                spec[ii] = ((fits.open(name)[0]).data)[0]
                
                #iterates for each spectra loaded
                ii = ii + 1 
                               
    return sol, spec

############################################################################################################

'''
findXPercentile function finds the (x,y) coordinates of the Xth percentile point in 20 bins of a spectra. This process is repeated for multiple spectra if given

findXPercentile(spectrumArray, percentile)
    spectrumArray is an list of Spectra to be analyzed. 
    percentile is an int from 0 to 100 that is the percentile level desired to be found (normally 90-100)
    
    
return allXLocList, allYLocList
    allXLocList is a list with the x coordinate value for the found Xth percentile point
    allYLocList         ''         y coordinate value             ''

'''


def findXPercentile(specList, percentile):
    
    #find the number of spectra to calculate fits for
    numOfSpec = len(specList)  
    
    #initialize the list of fitting point locations for each spectra
    allXLocList = [0]*numOfSpec
    allYLocList = [0]*numOfSpec
    
    #generate a sublist of 20 fitting points for each spectra 
    for ii in range (0,numOfSpec):
        thisSpec = specList[ii] #[0]  
        XLocList = [0]*20         #initalizes list to store x location of fitting points
        YLocList = [0]*20         #''' y location '''
        
        #Goes through the entire spectrum and splits up into 20 windows
        for jj in range (0,20):     
            startPoint = jj*200     
            endPoint = startPoint+100
            subSpec = thisSpec[startPoint:endPoint]       

            percHigh = np.percentile(subSpec, percentile)   
            percLow = np.percentile(subSpec, (percentile-1))  

            #find the x,y location of the fitting point
            oldY = 0
            for kk in range (0,100):           
                index = startPoint + kk      
                newY = thisSpec[index]       
                if newY >= percLow and newY > oldY and newY <= percHigh: 
                    XLocList[jj] = index     
                    YLocList[jj] = newY     
                 
        #record the 20 fitting points for each spectra
        allXLocList[ii] = XLocList
        allYLocList[ii] = YLocList
        
    return allXLocList, allYLocList



'''
unShiftSpecta fucntion is for doing a primary shift in the spectra
Notes:
-will crop to 200-3000 indicies, so the spectra must be longer than 3000+stepsToTake
-input spectra that have already been normalized

inputs:
Spec is the data list for a SINGLE spectra
refSpec is a referecnce spec to correlate Spec to
stepsToTake is the desired maximum number of steps up AND down to take in this process (usually pick about 100)

returns: 
maxCorSpec is the new slightly cropped and shifted spec
y is the complete list of correlations for every step up and down
yMax is the step that resulted in max correlation. Note that steps up will be stepsToTake + step and steps down will be stepsToTake - step

'''

def unShiftSpectra(Spec,refSpec,stepsToTake):
    
    
    #initialize values
    oldSpecUp = Spec
    oldSpecDown = Spec
    newSpecUp= Spec
    newSpecDown = Spec
    maxCorSpec = Spec
    maxCorSpecUp = Spec
    maxCorSpecDown = Spec
    refSpec = refSpec
    maxCorUp = 0
    maxCorDown = 0
    specSize = len(refSpec)
    
    
    #generates the low and high cut off points for the spectra
    lowCutOff = np.floor(200.*specSize/4021.)
    highCutOff = np.floor(3000.*specSize/4021.)

    
    y = np.zeros(stepsToTake*2)

    #find the step up or down that results in the maximum correlation to the reference spec
    for ii in range (0,stepsToTake):    
        
        #intialize lists to save new specra for each step  
        newSpecUp = np.zeros(specSize)
        newSpecDown = np.zeros(specSize)
            
        #go throught the spectra and make a step down
        for jj in range (0,specSize):  
             
            if jj+1 < specSize:              
                newSpecDown[jj] = oldSpecDown[jj+1]    
                
            #take care of the end point by using the orignal spectra
            elif jj+1 == specSize:
                newSpecDown[jj] = Spec[highCutOff-1+ii]
        
        #find the correlation between this new spectra and the reference and save in y
        correlation = (np.correlate(newSpecDown,refSpec))[0]  
        y[stepsToTake - ii - 1] = correlation

        #find the max correlation for steps down and save the spectra and correlation value
        if correlation == max(y):
            maxCorSpecDown = newSpecDown
            maxCorDown = correlation
        
        #complete the step looping by setting advancing the step
        oldSpecDown = newSpecDown
    ########################################################
    
    
        #go throught the spectra and take a step up
        for jj in range (0,specSize):   
            if jj == 0:
                newSpecUp[0] = Spec[lowCutOff-ii]  
            
            #take care of end conditions
            elif jj < specSize:          
                newSpecUp[jj] = oldSpecUp[jj-1]   
                
        #find the correlation between this new spectra and the reference and save in y
        correlation = (np.correlate(newSpecUp,refSpec))[0]  #check the shift correlation to the refernce 
        y[stepsToTake + ii] = correlation
       
        #find the max correlation for steps down and save the spectra and correlation value
        if correlation == max(y):
            maxCorSpecUp = newSpecUp
            maxCorUp = correlation
    
        #complete the step looping by setting advancing the step   
        oldSpecUp = newSpecUp

    
    
    #once all steps have been taken, find the max correlation for all up and down steps
    if maxCorUp > maxCorDown:
    #print "up"
        maxCorSpec = maxCorSpecUp
    
    if maxCorUp < maxCorDown:
    #print "down"
        maxCorSpec = maxCorSpecDown
      
    if maxCorUp == maxCorDown:
        #print "0"
        maxCorSpec = maxCorSpecDown 
    
    
    #save the index of the max correlation step
    for ii in range (0,len(y)):     
        if y[ii] == max(y):
            yMax = ii
            #print ii
            
    return maxCorSpec, y, yMax

##################################################################

#fit spline function to each maxCorSpec around range of interest (2 A)
    

#discretize each spline to x10 points as before i.e. 1/10 of a pixel
#find max pixel location
#shift spectra to this level
'''
interpFunction is a function that interpolates a list and returns a list with a higher size over the same data

inputs:
    inputList is the list to be interpolated
    divison is the number of list indexes in the output list compared to the input list

returns:
    outputList is the input list that has been interpolated and subdevided into a new list with more entries

'''
    
def interpFunction(inputList, division):
    
    #intialize values for the interpolation calculation
    sizeOfWindow = len(inputList)
    x = np.linspace(0,sizeOfWindow,num=sizeOfWindow)
    
    #interpolate the inputList
    intFun = interp1d(x, inputList, kind='linear')
    
    #intitalize the output list
    outputList = np.zeros(sizeOfWindow*division) 
    
   #break up the interpolated function to make the output list 
    for ii in range(0,sizeOfWindow*division):
        outputList[ii] = intFun(ii*(division)**(-1))
        
        
    return outputList
    
    
##################################################################

'''
specShiftCrop function does a final shift and crop and to a window of interest around Halpha for a list of specs

inputs:
    specs- the list of spectra to be trimmed
    sol - the wavelength solution for the given spces
    maxCorr- the array of max correlation locations for each spec (calculated using unShiftSpectra)
    lenOfCorrInterp - the length of the correlation range, used for finding the shift from the value of the maxCorr
    
return:
    finalSpecs- the final cropped and shifted spectra in the new window of interest
    finalSol- the final wavlength solution for the EACH of the finalSpecs

'''
def specShiftCrop(specs, sol, maxCorrs, lenOfCorrInterp):
    
    #intialize values
    numOfSpecs = len(specs)
    cropSol = []
    cropSpec = [[]]*numOfSpecs
    
    print cropSpec
    print 'num of specs', numOfSpecs
    
    # cut the wavelength solution and specs to be around HAlpha
    jj = 0
    for ii in range(0,len(sol)):
        
        #take only values in the window
        if sol[ii] >= 6550 and sol[ii] <= 6600:
            
            #save the solution in this window
            cropSol.append(0)
            cropSol[jj] = sol[ii]
            
            #for all specs, crop to the same window as the solution
            for kk in range (0,numOfSpecs):
                cropSpec[kk].append(0)
                cropSpec[kk][jj] = specs[kk][ii]                
                
            jj = jj + 1
        
      
    
    lenOfWindow = len(cropSol)
    print 'len of window', lenOfWindow
    
    
    #intialize the final outputs
    finalSpecs = [[0]]*numOfSpecs
    finalSols = [[0]]*numOfSpecs
    
    
    #shift the cropped spectra using the correlation function from the input
    for ii in range(0,numOfSpecs):
        
        #begin by setting the final spec to the current cropped spec
        #finalSpecs[ii] = cropSpec[ii]
    
        #find the precise shift in units of the index for each spec
        print lenOfCorrInterp
        print len(cropSol)
        print 'maxcorrs',maxCorrs[0]
        shift = int((maxCorrs[ii]) - lenOfCorrInterp/2.)
        
        #shift the spectra
        jj = 0
        for kk in range (0,lenOfWindow):
            if (lenOfWindow - abs(shift)) > kk: 
                            
                finalSpecs[ii].append(cropSpec[ii][kk+shift])
                finalSols[ii].append(cropSol[kk+shift])
 
   # print lenOf

    return finalSpecs, finalSols
