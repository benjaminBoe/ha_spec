'''
This code is designed to plot a 1.6 Angstrom window around the H alpha absorbtion line in spectra.
It loops through each spectra given and narrows down the spectra to a window of 5 angstroms then finds the minimum point of the absorbtion
It then creates the 1.6 A window around the minimum, plots the resulting spectra and summs up the values to find the flux
'''

import sys
sys.path.append('/Users/CommanderData/Documents/Grad_School/Fall_2015/RA/program_files/')

import SpecFun as SPF
reload(SPF)
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt  
from scipy.interpolate import interp1d


fileLocation = '/Users/CommanderData/Documents/Grad_School/Fall_2015/RA/Spectra_data/'   #type out folder location
sp = SPF.readInFiles(fileLocation)   #removes desired fits files folder into an array 
sol = sp[0]

specList = sp[1]
XYLoc = SPF.findXPercentile(specList,99)
#specList = [0]*10
#for ii in range (0,10):
  #  specList[ii] = sp[ii][0]

numOfSpec = len(specList)
print 'number of spec', numOfSpec
#indexPerAngst = len(sol)/(max(sol)-min(sol))
plotYN = True

normedSpecs = [0]*numOfSpec
for ii in range (0,numOfSpec):
    xLoc = XYLoc[0][ii]
    yLoc = XYLoc[1][ii]
    #print xLoc,yLoc
    spec = specList[ii]
    a = np.polyfit(xLoc,yLoc,6)      #creats the curve fit to 6th order polynomial
    solSize = len(sol)
    x = np.linspace(0,solSize,num = (solSize))    #creates x axis points for plotting with the 1.6A window size
    yplot = a[0]*x**6 + a[1]*x**5 + a[2]*x**4 + a[3]*x**3 + a[4]*x**2 + a[5]*x + a[6]    #written out equation for the polynomial fit
    normSpec = (spec)/(yplot)  
    normedSpecs[ii] = normSpec

 
    if plotYN == True and ii == 0:
        fig = plt.figure(frameon=False)   
        fig.set_size_inches(50,10)        #set figure size for high resolution viewing
        fig.title =  "Spectrum "+ str(ii) + ' Unnormalized'
        plt.xlabel('Wavelength, index', fontsize = 50)                 #setting axis labels for later plot
        plt.ylabel('Light Intensity', fontsize = 50)  
        plt.plot(x,spec)                            #plots the new shifted spectra
        plt.plot(xLoc,yLoc,'ko',ms=10)      #plots points to be used in the polynomial fit
        plt.plot(x,yplot)
        plt.ylim((0,200000))   
        plt.xlim((0,4000))
        fig2 = plt.figure(frameon=False)   
        fig2.set_size_inches(20,10)        #set figure size for high resolution viewing
    
        plt.plot(sol,normSpec)
        fig2.title =  "Spectrum "+ str(ii) + 'Normalized'
        plt.xlabel('Wavelength, A', fontsize = 50)                 #setting axis labels for later plot
        plt.ylabel('Light Intensity', fontsize = 50)  
        plt.ylim((0,1.5))   
        plt.xlim(6550,6590)
    
    
        y = np.ones(solSize)
        plt.plot(sol,y)
        
unShiftedSpec = []
amountOfCorr = []
interpCorr = []
finalSpecs = []
finalSol = []
stepsToTake = 100

for ii in range (0, numOfSpec): 
    unShiftedSpec.append(0) 
    amountOfCorr.append(0)
    interpCorr.append(0)
    finalSpecs.append(0)
    finalSol.append(0)
    
    spectraShift = (SPF.unShiftSpectra(normedSpecs[ii],normedSpecs[8],stepsToTake, sol))
    unShiftedSpec[ii] = spectraShift[0]
    amountOfCorr[ii] = spectraShift[1]
    interpCorr[ii] = spectraShift[2]
    finalSpecs[ii] = spectraShift[3]
    finalSol[ii] = spectraShift[4]


    #print ii
#print spectraShift[1] 
 
if plotYN == True:
    fig3 = plt.figure()
    for jj in range (0, numOfSpec):
        
        plt.xlim(6560,6565)
        plt.ylim((0,1.5))   
        plt.xlabel('Wavelength, index', fontsize = 50)                 #setting axis labels for later plot
        plt.ylabel('Light Intensity', fontsize = 50)  
        fig3.set_size_inches(10,10)
        plt.plot(sol[200:3000],unShiftedSpec[jj])
    
    
    fig4 = plt.figure()
    x = np.linspace(-stepsToTake,stepsToTake, num = 2*stepsToTake)
    for jj in range (0, numOfSpec):
        y = amountOfCorr[jj]
        plt.plot(x,y)
        plt.ylim(min(y) - 100, max(y) + 100)
        plt.show()


    fig5 = plt.figure()
    sizeOfWindow = len(interpCorr[0])
    x = np.linspace(0,sizeOfWindow, num = sizeOfWindow)
    for jj in range (0, numOfSpec):
        y = interpCorr[jj]
        plt.plot(x,y)
        
        
    fig6 = plt.figure()
    for jj in range (0, numOfSpec): 
        plt.xlim(6560,6565)
        plt.ylim((0,1.5))   
        plt.xlabel('Wav, index', fontsize = 50)                 #setting axis labels for later plot
        plt.ylabel('Light Intensity', fontsize = 50)  
        fig6.set_size_inches(10,10)
        plt.plot(finalSol[jj],finalSpecs[jj])
        
        plt.show()


