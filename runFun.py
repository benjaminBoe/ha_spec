'''
This code is designed to plot a 1.6 Angstrom window around the H alpha absorbtion line in spectra.
It loops through each spectra given and narrows down the spectra to a window of 5 angstroms then finds the minimum point of the absorbtion
It then creates the 1.6 A window around the minimum, plots the resulting spectra and summs up the values to find the flux
'''

import sys
sys.path.append('/Users/CommanderData/Documents/Grad_School/Fall_2015/RA/program_files/')

import SpecFun as SPF
reload(SPF)
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt  
from scipy.interpolate import interp1d

'''
runCode is a function to run the procedure for normalizing, unshifting, and cropping spectra around Halpha

inputs:
    fileLocation- string that contains the path to the folder with the desiresd spectra
    percentile- the desired percentile to set the normalization points by, default 99th percentile
    refNum- the index of the spectra to be used as a reference for shifting
    stepsToTake- the deisred number of steps up and down to try for the first shift
    
outputs:
     finalSpecs- the final unshifted, normalized, cropped spectra
     finalSol- wavelength solution for the finalSpecs
     specList- orignal list of spectra from the data
     sol- is the original wavlength solution from the data
     yplot - list of 6th order polynomials evaluated across the original spectra indecies
     normSpec - the normalized spectra list (uses sol for soluton)
     stepsToTake - number of steps that were taken for the unshifting fit
     amountOfCorr - list of correlation values for the first shift
     maxCorr - the interpolated correlation values from amountOfCorr
'''

def runCode(fileLocation, percentile = 99, refNum = 1, stepsToTake = 100):
    
    #removes desired fits files folder into an array 
    sp = SPF.readInFiles(fileLocation)   
    
    #removes the wavlength solution
    sol = sp[0]

    #removes the list of spectra
    specList = sp[1]
    
    #find the x and y locaitons for the fitting points of each spectra
    XYLoc = SPF.findXPercentile(specList,percentile)
    
    #initalize values
    numOfSpec= len(specList)
    normedSpecs = [0]*numOfSpec
    yplot = [0]*numOfSpec
    solSize = len(sol)
    x = np.linspace(0,solSize,num = (solSize))    
    
    #use percentile points to normalize each spectra
    for ii in range (0,numOfSpec):
        #take out the x,y points
        xLoc = XYLoc[0][ii]
        yLoc = XYLoc[1][ii]
    
        #do a 6th order polynomial fit to the Xth percentile points
        a = np.polyfit(xLoc,yLoc,6)      
        
        #calculate the resuting plolynomial
        yplot[ii] = a[0]*x**6 + a[1]*x**5 + a[2]*x**4 + a[3]*x**3 + a[4]*x**2 + a[5]*x + a[6]  
        
        #normalize the spectra using the polynomial fit 
        normSpec = (specList[ii])/(yplot[ii])  
        normedSpecs[ii] = normSpec

    #interpolate the entire spectra to do the first shift
    highDefSpecs = []
    for ii in range (0, numOfSpec): 
        highDefSpecs.append(0)
        highDefSpecs[ii] = SPF.interpFunction(normedSpecs[ii],10)

    #intialize values
    unShiftedSpec = []
    amountOfCorr = []
    maxCorr = [0]*numOfSpec

    #perfom the shift for each spectra
    for ii in range (0, numOfSpec): 
        unShiftedSpec.append(0) 
        amountOfCorr.append(0)
        
        #get the shift info from the UnshiftSpec function
        spectraShift = (SPF.unShiftSpectra(highDefSpecs[ii],highDefSpecs[refNum],stepsToTake))
        
        #remove the unshifted spectra and the correlation data
        unShiftedSpec[ii] = spectraShift[0]
        amountOfCorr[ii] = spectraShift[1]
        maxCorr[ii] = spectraShift[2]
        
        #interpolate the correlation data
        corrInterp = SPF.interpFunction(amountOfCorr[ii], 100)
        lenOfCorrInterp = len(corrInterp)
        
        #devide up the correlation data and find the maximum point in the interpolated correlation function
        for jj in range(0,lenOfCorrInterp):
            if corrInterp[jj] == max(corrInterp):
                #print 'maxCorr for spec', ii ,'is ', corrInterp[jj]
                maxCorr[ii] = jj
    

    #do the final shift using the interpolated max of the correlation function
    finalShift = SPF.specShiftCrop(unShiftedSpec,sol,maxCorr,lenOfCorrInterp)
    finalSpecs = finalShift[0]
    finalSols = finalShift[1]


    return finalSpecs, finalSols, specList, sol, yplot, normSpec, stepsToTake, amountOfCorr
    
